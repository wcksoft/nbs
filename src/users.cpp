// WBS: users.cpp 0.1.0
// Desc.:
// (c) 2018 Wicked Software

#include <iostream>
#include <string>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/random_generator.hpp>
#include "users.h"
#include "db/sqlite.h"
#include "bot.h"

#ifndef DEBUG
 #define DEBUG true
#endif

using namespace std;

namespace WBS {
  users::users(db::sqlite*& db, WBS::Bot*& bot) {
    this->db = db;
    this->bot = bot;
  }
  users::users(void) {  }
  users::~users(void) {  }
  std::string users::version(void) { return "0.1.0"; }
  void users::debug(std::string data) { if (DEBUG) { if ((data != "\n") && (data != "\r") && (data != "\0")) { cout<<">>>DEBUG: "<<data; } } }
  std::string users::getUUID(void) {
    boost::uuids::random_generator gen;
    boost::uuids::uuid u = gen();
    return boost::uuids::to_string(u);
  }
  void users::init_tables(void) {
    this->init_table_users();
    this->init_table_users_access();
  }

  void users::init_table_users(void) {
    char *err = 0;
    string query = "CREATE TABLE IF NOT EXISTS users (\
                   id TEXT PRIMARY KEY,\
                   username TEXT NOT NULL UNIQUE,\
                   email TEXT NOT NULL UNIQUE,\
                   password TEXT NOT NULL);";
    this->db->query(query);
  }
  void users::init_table_users_access(void) {
    char *err = 0;
    string query = "CREATE TABLE IF NOT EXISTS users_access (\
                   uid TEXT PRIMARY KEY,\
                   channel INTEGER NULL,\
                   user INTEGER NULL,\
                   restart INTEGER NULL);";
    this->db->query(query);
  }
  void users::add(std::string user, std::string email, std::string pass) {
    char *err = 0;
    string query = "INSERT INTO users (name, email, pass)\
                   VALUES('"+user+"', '"+email+"', '"+pass+"');";
    this->db->query(query);
  }
  void users::del(std::string id) {
    char *err = 0;
    string query = "DELETE FROM users WHERE id LIKE '"+id+"';";
    this->db->query(query);
  }
  void users::edit(std::string name, std::string email="", std::string password="") {
    char *err = 0;
    string query = "INSERT INTO users (name, email, pass)\
                   VALUES('"+name+"', '"+email+"', '"+password+"')\
                   ON CONFLICT(name) DO UPDATE SET \
                   email="+email+",\
                   password="+password+";";
    this->db->query(query);
  }
  void users::edit_access(std::string id, int chan=0, int user=0, int restart=0) {
    char *err = 0;
    string query = "INSERT INTO users_access(uid, channel, user, restart) \
                   VALUES('"+id+"','"+to_string(chan)+"','"+to_string(user)+"','"+to_string(restart)+"')\
                   ON CONFLICT(uid) DO UPDATE SET \
                   channel="+to_string(chan)+",\
                   user="+to_string(user)+",\
                   restart="+to_string(restart)+";";
    this->db->query(query);
  }

}
