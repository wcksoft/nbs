// WBS: core.h 0.1.0
// Desc.: This is used for display purposes.
// (c) 2011-2018 Wicked Software

#ifndef _WBS_CORE_H_
#define _WBS_CORE_H_

#include <iostream>
#include <string>
#include <chrono>
#include "db/config.h"
#include "db/sqlite.h"
#include "lib/socket_client.h"
#include "lib/os.h"
#include "commands.h"
#include "commands/msg.h"
#include "commands/chan.h"
#include "events.h"
#include "parse.h"
#include "users.h"
#include "channels.h"
#include "seen.h"

namespace WBS {
  class core {
   public:
    core(void);
    ~core(void);
    std::string version(void);
    std::string read_socket(void);
    void debug(std::string);
    WBS::parse *parse;
    WBS::Bot *bot;
    WBS::Commands *send;
    WBS::users *users;
    WBS::channels *channels;
    void init_tables(void);

   private:
    Socket::client *sock;
    WBS::Events *event;
    db::sqlite *sql;
    WBS::msgcom *msg;
    WBS::chancom *chan;
    WBS::seen *seen;
    //__int64 tsboot;
  };
};
#endif
