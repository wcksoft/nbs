// WBS: bot.cpp 0.2.0
// Desc.:
// (c) 2011-2018 Wicked Software

#include <iostream>
#include <string>
#include <cstring>
#include <sys/utsname.h>
#include <errno.h>
#include "bot.h"

#ifndef DEBUG
 #define DEBUG true
#endif

using namespace std;

namespace WBS {
  Bot::Bot(void) {
    this->botname = "wbs";
    this->identd = "wbs";
    this->realname = "Wicked Bot System 5";
    this->startchan = "#wbs";
    this->server = "irc.wcksoft.com";
    this->port = 6667;
  }
  Bot::~Bot(void) {  }
  std::string Bot::version(void) { return "0.2.0"; }
  void Bot::debug(std::string data) { if (DEBUG) { if ((data != "\n") && (data != "\r") && (data != "\0")) { cout<<">>>DEBUG: "<<data; } } }
  void Bot::show(std::string data) { if (DEBUG) { if ((data != "\n") && (data != "\r") && (data != "\0")) { cout<<data; } } }

  std::string Bot::get_botname(void) { return this->botname; }
  void Bot::set_botname(std::string botname) { this->botname = botname; }
  std::string Bot::get_identd(void) { return this->identd; }
  std::string Bot::get_realname(void) { return this->realname; }
  std::string Bot::get_startchan(void) { return this->startchan; }
  std::string Bot::get_server(void) { return this->server; }
  int Bot::get_port(void) { return this->port; }
  std::string Bot::kernel(void) {
    struct utsname *wbssys = new utsname();
    int ret = uname(wbssys);
    if (ret > -1) {
      return (std::string)wbssys->sysname+" "+(std::string)wbssys->release;
    } else {
      return "ERROR > "+(std::string)strerror(errno);
    }
  }
  std::string Bot::arch(void) {
    struct utsname *wbssys = new utsname();
    int ret = uname(wbssys);
    if (ret > -1) {
      return (std::string)wbssys->machine;
    } else {
      return "ERROR > "+(std::string)strerror(errno);
    }
  }
  std::string Bot::hostname(void) {
    struct utsname *wbssys = new utsname();
    int ret = uname(wbssys);
    if (ret > -1) {
      return (std::string)wbssys->nodename;
    } else {
      return "ERROR > "+(std::string)strerror(errno);
    }
  }
}
