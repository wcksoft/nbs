// WBS: msg.h 0.1.0
// Desc.: This is used for display purposes.
// (c) 2018 Wicked Software

#ifndef _WBS_PARSE_H_
#define _WBS_PARSE_H_

#include <iostream>
#include <string>
#include "lib/socket_client.h"
#include "commands.h"
#include "commands/msg.h"
#include "commands/chan.h"
#include "events.h"
#include "bot.h"

namespace WBS {
  class parse {
   public:
    parse(Socket::client*&, WBS::Commands*&, WBS::Events*&, WBS::msgcom*&, WBS::chancom*&, WBS::Bot*&);
    parse(void);
    ~parse(void);
    std::string version(void);
    void debug(std::string);
    void show(std::string);
    void work(std::string);
    std::string strip(std::string, char);

   private:
     Socket::client *sock;
     WBS::Commands *send;
     WBS::Events *event;
     WBS::msgcom *msg;
     WBS::chancom *chan;
     WBS::Bot *bot;
  };
};
#endif
