// WBS: channels.cpp 0.1.0
// Desc.:
// (c) 2018 Wicked Software

#include <iostream>
#include <string>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/random_generator.hpp>
#include "channels.h"
#include "db/sqlite.h"
#include "bot.h"
#include "commands.h"

#ifndef DEBUG
 #define DEBUG true
#endif

using namespace std;

namespace WBS {
  channels::channels(db::sqlite*& db, WBS::Bot*& bot, WBS::Commands*& send) {
    this->db = db;
    this->bot = bot;
    this->send = send;
  }
  channels::channels(void) {  }
  channels::~channels(void) {  }
  std::string channels::version(void) { return "0.1.0"; }
  void channels::debug(std::string data) { if (DEBUG) { if ((data != "\n") && (data != "\r") && (data != "\0")) { cout<<">>>DEBUG: "<<data; } } }
  std::string channels::getUUID(void) {
    boost::uuids::random_generator gen;
    boost::uuids::uuid u = gen();
    return boost::uuids::to_string(u);
  }
  void channels::init_tables(void) {
    this->init_table_chans();
    this->init_table_chans_access();
  }
  void channels::init_table_chans(void) {
    char *err = 0;
    string query = "CREATE TABLE IF NOT EXISTS chans (\
                   id TEXT PRIMARY KEY,\
                   name TEXT NOT NULL UNIQUE,\
                   key TEXT NULL);";
    this->db->query(query);
  }
  void channels::init_table_chans_access(void) {
    char *err = 0;
    string query = "CREATE TABLE IF NOT EXISTS chans_access (\
                   uid TEXT,\
                   cid TEXT,\
                   PRIMARY KEY (uid, cid),\
                   user INTEGER NULL,\
                   ban INTEGER NULL,\
                   options INTEGER NULL);";
    this->db->query(query);
  }

  void channels::add(std::string chan, std::string key="") {
    char *err = 0;
    if (key == "") {
      key = "NULL";
    } else {
      key = "'"+key+"'";
    }
    string query = "INSERT INTO chans (name, key)\
                   VALUES('"+chan+"', "+key+");";
    this->db->query(query);
  }
  void channels::del(std::string id) {
    char *err = 0;
    string query = "DELETE FROM chans WHERE id LIKE '"+id+"';";
    this->db->query(query);
  }
  void channels::edit(std::string chan, std::string key="") {
    char *err = 0;
    if (key == "") { key = "NULL"; }
    string query = "INSERT INTO chans (name, key)\
                   VALUES('"+chan+"', '"+key+"')\
                   ON CONFLICT(name) DO UPDATE SET \
                   key="+key+";";
    this->db->query(query);
  }
  void channels::edit_access(std::string uid, std::string cid, int user=0, int ban=0, int options=0) {
    char *err = 0;
    string query = "INSERT INTO chans_access(uid, cid, user, ban, options) \
                   VALUES('"+uid+"','"+cid+"','"+to_string(user)+"','"+to_string(ban)+"','"+to_string(options)+"')\
                   ON CONFLICT(uid,cid) DO UPDATE SET \
                   user="+to_string(user)+",\
                   ban="+to_string(ban)+",\
                   options="+to_string(options)+";";
    this->db->query(query);
  }
  void channels::join_all(void) {

  }


  int channels::count(void) {

  }
}
