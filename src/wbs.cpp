// Wicked Bot System - main.cpp 5.0.0
// Desc.: This is been coded as a follow up of the script that once ran on eggdrop.
// Req.:
// (c) 2012-2018 Wicked Software


#include <iostream>
#include <string>
#include <chrono>
#include "core.h"

#define CONFFILE (std::string)"wbs.conf"
#define DEBUG false

const std::string server = "irc.wcksoft.com";
int port = 6667;
const std::string chan = "#tohands";
std::chrono::milliseconds sleep(500);


int main () {
  WBS::core *wbs = new WBS::core();
  std::string data;
  bool daemon = true;
  if (wbs->send->connect(wbs->bot->get_server(), wbs->bot->get_port())) {
    wbs->send->user(wbs->bot->get_botname(), wbs->bot->get_identd(), "wck", wbs->bot->get_realname());
    wbs->send->nick(wbs->bot->get_botname());
    //wbs->send->join(wbs->bot->get_startchan());
    bool firsttime = true;
    while (daemon) {
      if (firsttime) {
        firsttime = false;
        //debug("Starting daemon loop..");
      }
      data = wbs->read_socket();
      wbs->debug(data);
      if (data == "SOCKERRTIMEOUT") { daemon = false; }
      if (data == "SOCKRECVFAILED") { daemon = false; }
      wbs->parse->work(data);
    }
    if (!daemon) {
      wbs->debug("Bot was stopped.\n");
      if (data == "SOCKERRTIMEOUT") { wbs->debug("Socket timed out.\n"); }
      if (data == "SOCKRECVFAILED") { wbs->debug("Socket receive failed.\n"); }
    } else {
      wbs->debug("Bot stopped for no apparent reasons.\n");
    }
  } else {
    wbs->debug("Could not connect to server.\n");
  }
  return 0;
}
