// WBS - socket_client.cpp 0.1.0
// Desc.: handles socket client connection with line by line feed for IRC support.
// (c) 2018 Wicked Software
#include <iostream>	//cout
#include <cstdio> //printf
#include <string>	//string
#include <cstring> //strlen
#include <sys/socket.h>	//socket
#include <arpa/inet.h>	//inet_addr
#include <netdb.h>	//hostent
#include "socket_client.h"

#ifndef DEBUG
 #define DEBUG true
#endif

using namespace std;


//	TCP Client class
namespace Socket {
  client::client() {
  	this->sock = -1;
  	port = 0;
  	address = "";
    this->buffer_active = false;
    this->buffer_index = 0;
    this->buffer_max = 1024;
    this->buffer_line = "";
    this->buffer_line_reset = true;
  }
  client::~client() {}
  std::string client::version(void) { return "0.1.0"; }

  std::string client::strip(std::string text, char delchar) {
    const char * chartxt = text.c_str();
    string newtxt = "";
    if (strlen(chartxt) > 0) {
     for (int i = 0; chartxt[i] != '\0'; i++) {
       if (chartxt[i] != delchar) { newtxt += chartxt[i]; }
     }
    }
    return newtxt;
  }

  void client::debug(std::string data) {
    if (DEBUG) {
      if ((data != "\n") && (data != "\r") && (data != "\0")) {
        cout<<">>>DEBUG: "<<this->strip(this->strip(data, '\r'), '\n')<<endl;
      }
    }
  }

  //	Connect to a host on a certain port number
  bool client::conn(string address , int port) {
    struct sockaddr_in server;
  	//create socket if it is not already created
  	if (sock == -1) {
  		//Create socket
  		this->sock = socket(AF_INET, SOCK_STREAM, 0);
  		if (this->sock == -1)	{
  			perror("Could not create socket");
        //return 0;
  		}
  		this->debug("Socket created\n");
      //return 1;
  	}

  	//setup address structure
  	if (inet_addr(address.c_str()) == -1) {
      cout<<"address is a host\n";
  		struct hostent *he;
  		struct in_addr **addr_list;

  		//resolve the hostname, its not an ip address
  		if ((he = gethostbyname(address.c_str())) == NULL) {
  			//gethostbyname failed
  			herror("gethostbyname");
  			this->debug("Failed to resolve hostname\n");
  			return false;
  		}

  		//Cast the h_addr_list to in_addr , since h_addr_list also has the ip address in long format only
  		addr_list = (struct in_addr **) he->h_addr_list;

  		for (int i = 0; addr_list[i] != NULL; i++)	{
  			//strcpy(ip , inet_ntoa(*addr_list[i]) );
  			server.sin_addr = *addr_list[i];
  			if (DEBUG) { cout<<address<<" resolved to "<<inet_ntoa(*addr_list[i])<<endl; }
  			break;
  		}
  	}	else {
      	//plain ip address
        this->debug("address is an ip\n");
  		server.sin_addr.s_addr = inet_addr(address.c_str());
  	}

  	server.sin_family = AF_INET;
  	server.sin_port = htons(port);

  	//Connect to remote server
  	if (connect(sock, (struct sockaddr *)&server, sizeof(server)) < 0)	{
  		perror("connect failed. Error");
  		return false;
  	}

  	this->debug("Connected\n");
  	return true;
  }

  //	Send data to the connected host
  bool client::send_data(string data) {
  	//Send some data
  	if (send(sock, data.c_str(), strlen(data.c_str()), 0) < 0) {
  		perror("Send failed : ");
  		return false;
  	}
  	this->debug(" >> "+data);
  	return true;
  }

  //	Receive data from the connected host
  string client::receive(int size=512) {
  	char buffer[size];
  	string reply;

  	//Receive a reply from the server
  	if (recv(sock, buffer, sizeof(buffer) , 0) < 0) {
  		this->debug("recv failed");
  	}

  	reply = buffer;
  	return reply;
  }

  void client::reset_buffer(void) {
    this->buffer_active = false;
    this->buffer_index = 0;
    this->buffer_max = 1024;
  }

  string client::get_line() {
  	//Receive a reply from the server
    if (!this->buffer_active) {
      this->buffer_max = recv(this->sock, this->buffer, 1024, MSG_DONTWAIT);
      //if (this->buffer_max < 0) {	puts("SOCKRECVFAILED"); return line; }
      if (this->buffer_max < 0) { this->reset_buffer(); return ""; }
      if (this->buffer_max == 0) { this->reset_buffer(); return "SOCKERRTIMEOUT"; }
      if (DEBUG) { cout<<"\n---> Got data from server. ("<<this->buffer_max<<" bytes) - Max: "<<this->buffer_max<<endl; }
      this->buffer_active = true;
      this->buffer_index = 0;
      if (this->buffer_line_reset) {
        this->buffer_line = "";
      } else {
        this->buffer_line_reset = true;
      }
    }
    //cout<<buffer<<endl;
    while ((this->buffer_index <= this->buffer_max) && (this->buffer_index <= 1024)) {
      if (this->buffer[this->buffer_index] == '\0') {
        this->reset_buffer();
        string line = this->buffer_line;
        line += "\n";
        this->buffer_line = "";
        return line;
      }
      if (this->buffer[this->buffer_index] == '\n') {
        this->buffer_index++;
        if ((this->buffer_index == 1025) || (this->buffer_index == this->buffer_max)) { this->reset_buffer(); }
        string line = this->buffer_line;
        line += "\n";
        this->buffer_line = "";
        return line;
      }
      if (this->buffer[this->buffer_index] == '\r') {
        this->buffer_index++;
        if ((this->buffer_index == 1025) || (this->buffer_index == this->buffer_max)) { this->reset_buffer(); }
        string line = this->buffer_line;
        line += "\n";
        this->buffer_line = "";
        return line;
      }
      this->buffer_line += this->buffer[this->buffer_index];
      this->buffer_index++;
      if ((this->buffer_index == 1025) || (this->buffer_index == this->buffer_max)) {
        this->buffer_line_reset = false;
        this->reset_buffer();
        return this->get_line();
      }
    }
    return this->buffer_line;
  }
/*
  int main(int argc, char *argtcp_v[]) {
  	tcp_client c;
  	string host;

  	cout<<"Enter hostname : ";
  	cin>>host;

  	//connect to host
  	c.conn(host , 80);

  	//send some data
  	c.send_data("GET / HTTP/1.1\r\n\r\n");

  	//receive and echo reply
  	cout<<"----------------------------\n\n";
  	cout<<c.receive(1024);
  	cout<<"\n\n----------------------------\n\n";

  	//done
  	return 0;
  }*/
};
