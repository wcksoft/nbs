// os.cpp 0.1.4
// Desc.: Detect the Linux distribution name and version number.
// (c) 2011-2012 Wicked Software

#include "os.h"
#include <iostream>
#include <fstream>
#include <string>

OS::OS(void) {
  OS::name = "Unknown OS";
}

OS::~OS(void) { }

std::string OS::version(void) {
 return "0.1.4";
}

std::string OS::getname(void) {
 return OS::name;
}

std::string OS::getdistro(void) {
 std::string gdistrotmp = OS::name;
 gdistrotmp.append(" ");
 gdistrotmp.append(OS::ver);
 return gdistrotmp;
}

std::string OS::check(void) {
 std::fstream verfile("/proc/version",std::ios::in);
 getline(verfile, OS::ver);
 verfile.close();
 checkDebian();
 checkRedhat();
 return OS::ver;
}

//Debian: /etc/debian_version, /etc/debian_release (rare)
int OS::checkDebian(void) {
 int result = OS::ver.find("Debian");
 if (result != std::string::npos) {
  OS::name = "Debian";
  std::fstream verfile("/etc/debian_version",std::ios::in);
  getline(verfile, OS::ver);
  verfile.close();
  OS::ver = getdistro();
  return 1;
 } else {
  return 0;
 }
}

//Red Hat: /etc/redhat-release, /etc/redhat_version (rare)
// Many non-Red Hat RPM-based distributions include /etc/redhat-release for compatibility.
int OS::checkRedhat(void) {
 std::string rhtmp;
 std::fstream verfile("/etc/redhat-release",std::ios::in);
 getline(verfile, rhtmp);
 verfile.close();
 if (rhtmp != "") {
  OS::name = "Redhat";
  OS::ver = rhtmp;
  return 1;
 } else {
  std::fstream verfile("/etc/redhat_version",std::ios::in);
  getline(verfile, rhtmp);
  verfile.close();
  if (rhtmp != "") {
   OS::name = "Redhat";
   OS::ver = rhtmp;
   return 1;
  } else {
   return 0;
  }
 }
}

//Annvix: /etc/annvix-release
int OS::checkAnnvix(void) {
 std::string rhtmp;
 std::fstream verfile("/etc/annvix-release",std::ios::in);
 getline(verfile, rhtmp);
 verfile.close();
 if (rhtmp != "") {
  OS::name = "Annvix";
  OS::ver = rhtmp;
  return 1;
 } else {
  return 0;
 }
}

//Arch Linux: /etc/arch-release
int OS::checkArch(void) {
 return 0;
}

//Arklinux: /etc/arklinux-release
int OS::checkArklinux(void) {
 return 0;
}

//Aurox Linux: /etc/aurox-release
int OS::checkAurox(void) {
 return 0;
}

//BlackCat: /etc/blackcat-release
int OS::checkBlackcat(void) {
 return 0;
}

//Cobalt: /etc/cobalt-release
int OS::checkCobalt(void) {
 return 0;
}

//Conectiva: /etc/conectiva-release
int OS::checkConectiva(void) {
 return 0;
}

//Fedora Core: /etc/fedora-release
int OS::checkFedora(void) {
 return 0;
}

//Gentoo Linux: /etc/gentoo-release
int OS::checkGentoo(void) {
 return 0;
}

//Immunix: /etc/immunix-release
int OS::checkImmunix(void) {
 return 0;
}

//Knoppix: knoppix_version
int OS::checkKnoppix(void) {
 return 0;
}

//Linux-From-Scratch: /etc/lfs-release
int OS::checkLFS(void) {
 return 0;
}

//Linux-PPC: /etc/linuxppc-release
int OS::checkLinuxPPC(void) {
 return 0;
}

//Mandrake: /etc/mandrake-release
int OS::checkMandrake(void) {
 return 0;
}

//Mandriva/Mandrake Linux: /etc/mandriva-release, /etc/mandrake-release, /etc/mandakelinux-release
int OS::checkMandriva(void) {
 return 0;
}

//MkLinux: /etc/mklinux-release
int OS::checkMklinux(void) {
 return 0;
}

//Novell Linux Desktop: /etc/nld-release
int OS::checkNovell(void) {
 return 0;
}

//PLD Linux: /etc/pld-release
int OS::checkPLD(void) {
 return 0;
}

//Slackware: /etc/slackware-version, /etc/slackware-release (rare)
int OS::checkSlackware(void) {
 return 0;
}

//SME Server (Formerly E-Smith): /etc/e-smith-release
int OS::checkSME(void) {
 return 0;
}

//Solaris SPARC: /etc/release
int OS::checkSolaris(void) {
 return 0;
}

//Sun JDS: /etc/sun-release
int OS::checkSun(void) {
 return 0;
}

//SUSE Linux: /etc/SuSE-release, /etc/novell-release
//SUSE Linux ES9: /etc/sles-release
int OS::checkSUSE(void) {
 return 0;
}

//Tiny Sofa: /etc/tinysofa-release
int OS::checkTinysofa(void) {
 return 0;
}

//TurboLinux: /etc/turbolinux-release
int OS::checkTurbolinux(void) {
 return 0;
}

//Ubuntu Linux: /etc/lsb-release
int OS::checkUbuntu(void) {
 std::string rhtmp;
 std::fstream verfile("/etc/lsb",std::ios::in);
 getline(verfile, rhtmp);
 verfile.close();
 if (rhtmp != "") {
  OS::name = "Ubuntu";
  OS::ver = rhtmp;
  return 1;
 } else {
  return 0;
 }
}

//UltraPenguin: /etc/ultrapenguin-release
int OS::checkUltrapenguin(void) {
 std::string rhtmp;
 std::fstream verfile("/etc/ultrapenguin-release",std::ios::in);
 getline(verfile, rhtmp);
 verfile.close();
 if (rhtmp != "") {
  OS::name = "UltraPenguin";
  OS::ver = rhtmp;
  return 1;
 } else {
  return 0;
 }
}

//UnitedLinux: /etc/UnitedLinux-release (covers SUSE SLES8)
int OS::checkUnited(void) {
 std::string rhtmp;
 std::fstream verfile("/etc/UnitedLinux-release",std::ios::in);
 getline(verfile, rhtmp);
 verfile.close();
 if (rhtmp != "") {
  OS::name = "UnitedLinux";
  OS::ver = rhtmp;
  return 1;
 } else {
  return 0;
 }
}

//VA-Linux/RH-VALE: /etc/va-release
int OS::checkVA(void) {
 std::string rhtmp;
 std::fstream verfile("/etc/va-release",std::ios::in);
 getline(verfile, rhtmp);
 verfile.close();
 if (rhtmp != "") {
  OS::name = "VA-Linux";
  OS::ver = rhtmp;
  return 1;
 } else {
  return 0;
 }
}

//Yellow Dog: /etc/yellowdog-release
int OS::checkYellowdog(void) {
 std::string rhtmp;
 std::fstream verfile("/etc/yellowdog-release",std::ios::in);
 getline(verfile, rhtmp);
 verfile.close();
 if (rhtmp != "") {
  OS::name = "Yellow Dog";
  OS::ver = rhtmp;
  return 1;
 } else {
  return 0;
 }
}
