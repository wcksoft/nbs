// os.h 0.1.4
// Desc.: Detect the Linux distribution name and version number.
// (c) 2011-2012 Wicked Software

#ifndef OS_H
#define OS_H

#include <string>

class OS {
 public:
  std::string check(void);
  OS(void);
  ~OS(void);
  std::string getname(void);
  std::string getdistro(void);
  std::string version(void);
 private:
  int checkDebian(void);
  int checkUbuntu(void);
  int checkDSL(void);
  int checkSlackware(void);
  int checkSolaris(void);
  int checkFedora(void);
  int checkRedhat(void);
  int checkFreeBSD(void);
  int checkNetBSD(void);
  int checkOpenBSD(void);
  int checkMandrake(void);
  int checkMandriva(void);
  int checkSUSE(void);
  int checkAnnvix(void);
  int checkArch(void);
  int checkArklinux(void);
  int checkAurox(void);
  int checkBlackcat(void);
  int checkCobalt(void);
  int checkConectiva(void);
  int checkGentoo(void);
  int checkImmunix(void);
  int checkKnoppix(void);
  int checkLFS(void);
  int checkLinuxPPC(void);
  int checkMklinux(void);
  int checkNovell(void);
  int checkPLD(void);
  int checkSME(void);
  int checkSun(void);
  int checkTinysofa(void);
  int checkTurbolinux(void);
  int checkUltrapenguin(void);
  int checkUnited(void);
  int checkVA(void);
  int checkYellowdog(void);
  std::string name;
  std::string ver;
};
#endif