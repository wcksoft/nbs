// WBS - socket_client.h 0.1.0
// Desc.: handles socket client connection with line by line feed for IRC support.
// (c) 2018 Wicked Software

#ifndef _SOCKET_CLIENT_H_
#define _SOCKET_CLIENT_H_

#include <string>
#include <sys/socket.h>

namespace Socket {
  class client {
    private:
    	int sock;
    	std::string address;
    	int port;
      char buffer[1025];
      bool buffer_active;
      bool buffer_line_reset;
      int buffer_index;
      int buffer_max;
      std::string buffer_line;
      void reset_buffer(void);

    public:
    	client();
      ~client();
      std::string version(void);
    	bool conn(std::string, int);
    	bool send_data(std::string data);
    	std::string receive(int);
      std::string get_line(void);
      void debug(std::string);
      std::string strip(std::string, char);
  };
};
#endif
