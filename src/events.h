// WBS: events.h 0.1.0
// Desc.:
// (c) 2018 Wicked Software

#ifndef _WBS_EVENTS_H_
#define _WBS_EVENTS_H_

#include <iostream>
#include <string>
#include "bot.h"
#include "commands.h"
#include "channels.h"

namespace WBS {
  class Events {
   public:
    Events(WBS::Bot*&, WBS::Commands*&, WBS::channels*&);
    Events(void);
    ~Events(void);
    std::string version(void);
    void debug(std::string);
    void show(std::string);
    void join(std::string, std::string);
    void part(std::string, std::string);
    void quit(std::string, std::string);
    void mode(std::string, std::string, std::string);
    void kick(std::string, std::string, std::string, std::string);
    void op(std::string, std::string, std::string);
    void deop(std::string, std::string, std::string);
    void voice(std::string, std::string, std::string);
    void devoice(std::string, std::string, std::string);
    void ban(std::string, std::string, std::string);
    void unban(std::string, std::string, std::string);
    void topic(std::string, std::string, std::string);
    void raw001(std::string); void raw002(std::string); void raw003(std::string); void raw004(std::string); void raw005(std::string);
    void raw200(std::string); void raw201(std::string); void raw202(std::string); void raw203(std::string); void raw204(std::string);
    void raw205(std::string); void raw206(std::string); void raw207(std::string); void raw208(std::string); void raw209(std::string);
    void raw210(std::string); void raw211(std::string); void raw212(std::string); void raw213(std::string); void raw214(std::string);
    void raw215(std::string); void raw216(std::string); void raw217(std::string); void raw218(std::string); void raw219(std::string);
    void raw221(std::string); void raw231(std::string); void raw232(std::string); void raw233(std::string); void raw234(std::string);
    void raw235(std::string); void raw240(std::string); void raw241(std::string); void raw242(std::string); void raw243(std::string);
    void raw244(std::string); void raw246(std::string); void raw247(std::string); void raw250(std::string); void raw251(std::string);
    void raw252(std::string); void raw253(std::string); void raw254(std::string); void raw255(std::string); void raw256(std::string);
    void raw257(std::string); void raw258(std::string); void raw259(std::string); void raw261(std::string); void raw262(std::string);
    void raw263(std::string); void raw265(std::string); void raw266(std::string); void raw300(std::string); void raw301(std::string);
    void raw302(std::string); void raw303(std::string); void raw305(std::string); void raw306(std::string); void raw311(std::string);
    void raw312(std::string); void raw313(std::string); void raw314(std::string); void raw315(std::string); void raw316(std::string);
    void raw317(std::string); void raw318(std::string); void raw319(std::string); void raw321(std::string); void raw322(std::string);
    void raw323(std::string); void raw324(std::string);
    void raw325(std::string);
    void raw332(std::string);
    void raw333(std::string);
    void raw353(std::string);
    void raw366(std::string);
    void raw372(std::string);
    void raw375(std::string);
    void raw376(std::string);
    void raw381(std::string);
    void raw382(std::string);
    void raw383(std::string);
    void raw384(std::string);
    //void raw385(std::string);
    void raw391(std::string);
    void raw392(std::string);
    void raw393(std::string);
    void raw394(std::string);
    void raw395(std::string);
    void raw400(std::string);
    void raw401(std::string);
    void raw402(std::string);
    void raw403(std::string);
    void raw404(std::string);
    void raw405(std::string);
    void raw406(std::string);
    void raw407(std::string);
    void raw408(std::string);
    void raw409(std::string);
    void raw411(std::string);
    void raw412(std::string);
    void raw413(std::string);
    void raw414(std::string);
    void raw415(std::string);
    void raw421(std::string);
    void raw422(std::string);
    void raw423(std::string);
    void raw424(std::string);
    void raw431(std::string);
    void raw432(std::string);
    void raw433(std::string, std::string);
    void raw436(std::string);
    void raw437(std::string);
    void raw441(std::string);
    void raw442(std::string);
    void raw443(std::string);
    void raw444(std::string);
    void raw445(std::string);
    void raw446(std::string);
    void raw451(std::string);
    void raw461(std::string);
    void raw462(std::string);
    void raw463(std::string);
    void raw464(std::string);
    void raw465(std::string);
    void raw466(std::string);
    void raw467(std::string);
    void raw471(std::string);
    void raw472(std::string);
    void raw473(std::string);
    void raw474(std::string);
    void raw475(std::string);
    void raw476(std::string);
    void raw477(std::string);
    void raw478(std::string);
    void raw481(std::string);
    void raw482(std::string);
    void raw483(std::string);
    void raw484(std::string);
    void raw485(std::string);
    void raw491(std::string);
    void raw492(std::string);
    void raw501(std::string);
    void raw502(std::string);

   private:
     WBS::Bot *bot;
     WBS::Commands *send;
     WBS::channels *channels;
     std::string strip(std::string, char);
     void raw_strip3(std::string);
     void raw_strip4(std::string);
     void raw_strip5(std::string);
  };
};
#endif
