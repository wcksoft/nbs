// wbs.h 5.0.0
// Desc.: This is used for display purposes.
// (c) 2011-2018 Wicked Software

#ifndef _WBS_COMMANDS_H_
#define _WBS_COMMANDS_H_

#include <iostream>
#include <string>
#include "lib/socket_client.h"

namespace WBS {
  class Commands {
   public:
    Commands(Socket::client *&);
    Commands(void);
    ~Commands(void);
    std::string version(void);
    void debug(std::string);
    void show(std::string);
    void user(std::string, std::string, std::string, std::string);
    void nick(std::string);
    void server_pong(std::string);
    void join(std::string);
    void part(std::string);
    void quit(std::string);
    void op(std::string, std::string);
    void deop(std::string, std::string);
    void voice(std::string, std::string);
    void devoice(std::string, std::string);
    void ban(std::string, std::string);
    void unban(std::string, std::string);
    void kick(std::string, std::string, std::string);
    void mode(std::string, std::string);
    void whois(std::string);
    void whowas(std::string);
    void links(void);
    void msg(std::string, std::string);
    bool connect(std::string, int);

    void ctcp_version(std::string);
    void ctcp_ping(std::string, std::string);
    void ctcp_time(std::string);
    void ctcp_clientinfo(std::string);
    void ctcp_clientinfo_ci(std::string);
    void ctcp_clientinfo_ver(std::string);
    void ctcp_clientinfo_ping(std::string);
    void ctcp_clientinfo_time(std::string);
   private:
     Socket::client *sock;
  };
};
#endif
