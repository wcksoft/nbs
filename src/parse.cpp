// WBS: msg.cpp 0.1.0
// Desc.:
// (c) 2018 Wicked Software

#include <boost/algorithm/string.hpp>
#include <iostream>
#include <string>
#include <vector>
#include <cstring>
#include <ctime>
#include "lib/socket_client.h"
#include "commands.h"
#include "commands/msg.h"
#include "commands/chan.h"
#include "events.h"
#include "parse.h"
#include "bot.h"

#ifndef DEBUG
 #define DEBUG true
#endif
#ifndef WBS_CHANCMD_PREFIX
 #define WBS_CHANCMD_PREFIX (std::string)"!"
#endif
#ifndef WBS_MSGCMD_PREFIX
 #define WBS_MSGCMD_PREFIX (std::string)"."
#endif

using namespace std;

namespace WBS {
  parse::parse(Socket::client*& sock, WBS::Commands*& send, WBS::Events*& event, WBS::msgcom*& msg, WBS::chancom*& chan, WBS::Bot*& bot) {
    this->sock = sock;
    this->send = send;
    this->event = event;
    this->msg = msg;
    this->chan = chan;
    this->bot = bot;
  }
  parse::parse(void) {  }
  parse::~parse(void) {  }
  std::string parse::version(void) { return "0.1.0"; }
  void parse::debug(std::string data) { if (DEBUG) { if ((data != "\n") && (data != "\r") && (data != "\0")) { cout<<">>>DEBUG: "<<data; } } }
  void parse::show(std::string data) { if (DEBUG) { if ((data != "\n") && (data != "\r") && (data != "\0")) { cout<<data; } } }

  std::string parse::strip(std::string text, char delchar) {
    const char * chartxt = text.c_str();
    string newtxt = "";
    if (strlen(chartxt) > 0) {
     for (int i = 0; chartxt[i] != '\0'; i++) {
       if (chartxt[i] != delchar) { newtxt += chartxt[i]; }
     }
    }
    return newtxt;
  }

  void parse::work(std::string data) {
    std::vector<std::string> arr;
    boost::split(arr, data, [](char c){return c == ' ';});
    if ((arr.size() > 1) && (arr[0] == "PING")) {
      //this->debug("Got ping request.\n");
      this->send->server_pong(arr[1]);
    } else if ((arr.size() > 2) && (arr[1] == "PRIVMSG")) {
      if (this->bot->get_botname() == arr[2]) { //private msg
        if (arr[3].substr(0,10) == ":\001VERSION\001") { this->send->ctcp_version(arr[0]); }
        if (arr[3].substr(0,7) == ":\001TIME\001") { this->send->ctcp_time(arr[0]); }
        if (arr[3].substr(0,6) == ":\001PING") { if (arr.size() > 3) { this->send->ctcp_ping(arr[0], arr[4]); } }
        if (arr[3].substr(0,13) == ":\001CLIENTINFO\001") {
          this->send->ctcp_clientinfo(arr[0]);
        } else if (arr[3].substr(0,12) == ":\001CLIENTINFO") {
          if (arr.size() > 3) {
            if (arr[4].substr(0,7) == "VERSION") { this->send->ctcp_clientinfo_ver(arr[0]); }
            if (arr[4].substr(0,4) == "PING") { this->send->ctcp_clientinfo_ping(arr[0]); }
            if (arr[4].substr(0,10) == "CLIENTINFO") { this->send->ctcp_clientinfo_ci(arr[0]); }
            if (arr[4].substr(0,4) == "TIME") { this->send->ctcp_clientinfo_time(arr[0]); }
          }
        }
        if ((arr.size() > 2) && (arr[3].substr(0,2) == ":"+WBS_MSGCMD_PREFIX)) {
          string args = "";
          for (unsigned int i = 4; i < arr.size(); i++) { if (i > 4) { args += " "; } args += arr[i]; }
          this->msg->commands(arr[0].substr(1), arr[3].substr(1), this->strip(this->strip(args, '\n'), '\r'));
        }
      } else { //chan
        if (arr[3].substr(0,10) == ":\001VERSION\001") { this->send->ctcp_version(arr[2]); }
        if (arr[3].substr(0,7) == ":\001TIME\001") { this->send->ctcp_time(arr[2]); }
        if (arr[3].substr(0,6) == ":\001PING") { if (arr.size() > 3) { this->send->ctcp_ping(arr[2], arr[4]); } }
        if (arr[3].substr(0,13) == ":\001CLIENTINFO\001") {
          this->send->ctcp_clientinfo(arr[2]);
        } else if (arr[3].substr(0,12) == ":\001CLIENTINFO") {
          if (arr.size() > 3) {
            if (arr[4].substr(0,7) == "VERSION") { this->send->ctcp_clientinfo_ver(arr[2]); }
            if (arr[4].substr(0,4) == "PING") { this->send->ctcp_clientinfo_ping(arr[2]); }
            if (arr[4].substr(0,10) == "CLIENTINFO") { this->send->ctcp_clientinfo_ci(arr[2]); }
            if (arr[4].substr(0,4) == "TIME") { this->send->ctcp_clientinfo_time(arr[2]); }
          }
        }
        if ((arr.size() > 2) && (arr[3].substr(0,2) == ":"+WBS_CHANCMD_PREFIX)) {
          string args = "";
          for (unsigned int i = 4; i < arr.size(); i++) { if (i > 4) { args += " "; } args += arr[i]; }
          this->chan->commands(arr[0].substr(1), arr[2], arr[3].substr(1), this->strip(this->strip(args, '\n'), '\r'));
        }
      }
    } else if ((arr.size() > 2) && (arr[1] == "JOIN")) {
      this->event->join(arr[0], arr[2]);
    } else if ((arr.size() > 2) && (arr[1] == "001")) {
      this->event->raw001(data);
    } else if ((arr.size() > 2) && (arr[1] == "002")) {
      this->event->raw002(data);
    } else if ((arr.size() > 2) && (arr[1] == "003")) {
      this->event->raw003(data);
    } else if ((arr.size() > 2) && (arr[1] == "004")) {
      this->event->raw004(data);
    } else if ((arr.size() > 2) && (arr[1] == "005")) {
      this->event->raw005(data);
    } else if ((arr.size() > 2) && (arr[1] == "200")) {
      this->event->raw200(data);
    } else if ((arr.size() > 2) && (arr[1] == "201")) {
      this->event->raw201(data);
    } else if ((arr.size() > 2) && (arr[1] == "202")) {
      this->event->raw202(data);
    } else if ((arr.size() > 2) && (arr[1] == "203")) {
      this->event->raw203(data);
    } else if ((arr.size() > 2) && (arr[1] == "204")) {
      this->event->raw204(data);
    } else if ((arr.size() > 2) && (arr[1] == "205")) {
      this->event->raw205(data);
    } else if ((arr.size() > 2) && (arr[1] == "206")) {
      this->event->raw206(data);
    } else if ((arr.size() > 2) && (arr[1] == "207")) {
      this->event->raw207(data);
    } else if ((arr.size() > 2) && (arr[1] == "208")) {
      this->event->raw208(data);
    } else if ((arr.size() > 2) && (arr[1] == "209")) {
      this->event->raw209(data);
    } else if ((arr.size() > 2) && (arr[1] == "210")) {
      this->event->raw210(data);
    } else if ((arr.size() > 2) && (arr[1] == "211")) {
      this->event->raw211(data);
    } else if ((arr.size() > 2) && (arr[1] == "212")) {
      this->event->raw212(data);
    } else if ((arr.size() > 2) && (arr[1] == "213")) {
      this->event->raw213(data);
    } else if ((arr.size() > 2) && (arr[1] == "214")) {
      this->event->raw214(data);
    } else if ((arr.size() > 2) && (arr[1] == "215")) {
      this->event->raw215(data);
    } else if ((arr.size() > 2) && (arr[1] == "216")) {
      this->event->raw216(data);
    } else if ((arr.size() > 2) && (arr[1] == "217")) {
      this->event->raw217(data);
    } else if ((arr.size() > 2) && (arr[1] == "218")) {
      this->event->raw218(data);
    } else if ((arr.size() > 2) && (arr[1] == "219")) {
      this->event->raw219(data);
    } else if ((arr.size() > 2) && (arr[1] == "250")) {
      this->event->raw250(data);
    } else if ((arr.size() > 2) && (arr[1] == "251")) {
      this->event->raw251(data);
    } else if ((arr.size() > 2) && (arr[1] == "252")) {
      this->event->raw252(data);
    } else if ((arr.size() > 2) && (arr[1] == "254")) {
      this->event->raw254(data);
    } else if ((arr.size() > 2) && (arr[1] == "255")) {
      this->event->raw255(data);
    } else if ((arr.size() > 2) && (arr[1] == "265")) {
      this->event->raw265(data);
    } else if ((arr.size() > 2) && (arr[1] == "266")) {
      this->event->raw266(data);
    } else if ((arr.size() > 2) && (arr[1] == "332")) {
      this->event->raw332(data); // needs updating. chan topic on join
    } else if ((arr.size() > 2) && (arr[1] == "333")) {
      this->event->raw333(data); // needs updating. chan ts on join
    } else if ((arr.size() > 2) && (arr[1] == "353")) {
      this->event->raw353(data); // needs updating. /names
    } else if ((arr.size() > 2) && (arr[1] == "366")) {
      this->event->raw366(data); // end /names
    } else if ((arr.size() > 2) && (arr[1] == "372")) {
      this->event->raw372(data);
    } else if ((arr.size() > 2) && (arr[1] == "375")) {
      this->event->raw375(data);
    } else if ((arr.size() > 2) && (arr[1] == "376")) {
      this->event->raw376(data);
    } else if ((arr.size() > 3) && (arr[1] == "433")) {
      this->event->raw433(arr[2], arr[3]);
    }
  }

}
