// wbs.cpp 5.0.0
// Desc.:
// (c) 2011-2018 Wicked Software

#include <cstdio>
#include <sys/socket.h>
#include <cstdlib>
#include <netinet/in.h>
#include <boost/algorithm/string.hpp>
#include <iostream>
#include <string>
#include <sys/types.h>
#include <unistd.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <vector>
#include <cstring>
#include <netinet/in.h>
#include <sstream>
#include <ctime>
#include <chrono>

#include "core.h"
#include "bot.h"
#include "db/config.h"
#include "db/sqlite.h"
#include "lib/socket_client.h"
#include "lib/os.h"
#include "commands.h"
#include "commands/msg.h"
#include "commands/chan.h"
#include "events.h"
#include "parse.h"
#include "users.h"
#include "channels.h"
#include "seen.h"

#ifndef DEBUG
 #define DEBUG true
#endif


using namespace std;

namespace WBS {
  core::core(void) {
    this->sock = new Socket::client();
    this->sql = new db::sqlite();
    this->bot = new WBS::Bot();
    this->send = new WBS::Commands(this->sock);
    this->users = new WBS::users(this->sql, this->bot);
    this->seen = new WBS::seen(this->sql, this->bot);
    this->channels = new WBS::channels(this->sql, this->bot, this->send);
    this->event = new WBS::Events(this->bot, this->send, this->channels);
    this->msg = new WBS::msgcom(this->sock, this->send, this->bot);
    this->chan = new WBS::chancom(this->sock, this->send, this->bot);
    this->parse = new WBS::parse(this->sock, this->send, this->event, this->msg, this->chan, this->bot);
    this->init_tables();
    //this->tsboot = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
  }
  core::~core(void) {  }
  std::string core::version(void) { return "5.0.0"; }
  std::string core::read_socket(void) { return this->sock->get_line(); }
  void core::debug(std::string data) { if (DEBUG) { if ((data != "\n") && (data != "\r") && (data != "\0")) { cout<<">>>DEBUG: "<<data; } } }

  void core::init_tables(void) {
    this->users->init_tables();
    this->channels->init_tables();
  }

}
