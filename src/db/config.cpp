// WBS: config.cpp 0.1.0
// Desc.:
// (c) 2018 Wicked Software

#include <string>
#include <fstream>
#include "config.h"
#include "configfile.hpp"

#define CONFFILE (string)ROOTPATH+"/etc/wbs.conf"

using namespace std;


namespace WBS {
  Config::Config(void) {  }

  Config::~Config(void) {  }

  void Config::create(void) {
    std::ofstream outfile(CONFFILE);
    string conf;
    conf = "name=wck\n";
    conf += "identd=wck\n";
    conf += "hostname=wck\n";
    conf += "realname=Wicked Bot System\n";
    conf += "server=irc.wcksoft.com\n";
    conf += "port=6667\n";
    //options
    conf += "chanlock=on\n";
    conf += "bitch=on\n";
    conf += "greet=on\n";
    conf += "seen=on\n";
    conf += "limit=on\n";
    conf += "topiclock=on\n";
    conf += "vcom=on\n";
    conf += "botnet=on\n";
    outfile<<conf;
    outfile.close();
  }

  bool Config::check(void) {
    std::ifstream file;
    string path = CONFFILE;
    file.open(path.c_str());
    if (!file) { this->create(); }
    file.open(path.c_str());
    if (!file) { return false; }
    return true;
  }

  string Config::get(string name) {
    if (this->check()) {
      ConfigFile conf(CONFFILE);
      if (conf.keyExists(name)) { return conf.getValueOfKey<std::string>(name); }
    }
    return "";
  }

  string Config::set(string name, string value) {
    return "";
  }

}
