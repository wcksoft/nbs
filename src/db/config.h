// WBS: config.h 0.1.0
// Desc.:
// (c) 2018 Wicked Software

#ifndef _CONFIG_H_
#define _CONFIG_H_

#include <iostream>
#include <string>

namespace WBS {
  class Config {
   public:
    Config(void);
    ~Config(void);
    std::string version(void);
    void create(void);
    bool check(void);
    std::string get(std::string);
    std::string set(std::string, std::string);

   private:

  };
};
#endif
