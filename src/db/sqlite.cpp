// WBS: sqlite.cpp 0.2.0
// Desc.:
// (c) 2018 Wicked Software

#include <string>
#include <sys/stat.h>
#include "sqlite.h"

#ifndef DEBUG
 #define DEBUG true
#endif

using namespace std;

namespace db {
  sqlite::sqlite(void) {
    //if (!this->exists("wbs.db")) { loadtables = true; }
    this->dbfile = "wbs.db";
    sqlite3_open(this->dbfile.c_str(), &this->_db);
    //if (loadtables) { this->init_tables(); }
  }
  sqlite::sqlite(std::string filename) {
    //if (!this->exists(filename)) { loadtables = true; }
    this->dbfile = filename;
    sqlite3_open(this->dbfile.c_str(), &this->_db);
    //if (loadtables) { this->init_tables(); }
  }
  sqlite::~sqlite(void) {  }
  std::string sqlite::version(void) { return "0.2.0"; }

  bool sqlite::exists(std::string name) {
    struct stat buffer;
    return (stat (name.c_str(), &buffer) == 0);
  }
  void sqlite::query(std::string query) {
    char *err = 0;
    sqlite3_exec(this->_db, query.c_str(), 0, 0, &err);
  }
  //void sqlite::query_callback(std::string query, std::string callback) {
  //  char *err = 0;
  //  sqlite3_exec(this->_db, query.c_str(), 0, 0, &err);
  //}
};
