// WBS: sqlite.h 0.2.0
// Desc.:
// (c) 2018 Wicked Software

#ifndef _WCK_DB_SQLITE_H_
#define _WCK_DB_SQLITE_H_

#include <string>
#include <sqlite3.h>

namespace db {
  class sqlite {
   public:
    sqlite(void);
    sqlite(std::string);
    ~sqlite(void);
    std::string version(void);
    void init_tables(void);
    void query(std::string);
    bool exists(std::string);

   private:
    sqlite3* _db;
    std::string dbfile;
  };
};
#endif
