// WBS: users.h 0.1.0
// Desc.: This is used for display purposes.
// (c) 2018 Wicked Software

#ifndef _WBS_USERS_H_
#define _WBS_USERS_H_

#include <iostream>
#include <string>
#include "db/sqlite.h"
#include "bot.h"

namespace WBS {
  class users {
   public:
    users(db::sqlite*&, WBS::Bot*&);
    users(void);
    ~users(void);
    std::string version(void);
    void debug(std::string);
    void add(std::string, std::string, std::string);
    void del(std::string);
    void edit(std::string, std::string, std::string);
    void edit_access(std::string, int, int, int);
    bool login(std::string, std::string);
    bool logout(std::string, std::string);
    void init_tables(void);

   private:
     db::sqlite *db;
     WBS::Bot *bot;
     std::string getUUID(void);
     void init_table_users(void);
     void init_table_users_access(void);
  };
};
#endif
