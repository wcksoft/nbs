// WBS: seen.h 0.1.0
// Desc.:
// (c) 2018 Wicked Software

#ifndef _WBS_SEEN_H_
#define _WBS_SEEN_H_

#include <iostream>
#include <string>
#include "db/sqlite.h"
#include "bot.h"

namespace WBS {
  class seen {
   public:
    seen(db::sqlite*&, WBS::Bot*&);
    seen(void);
    ~seen(void);
    std::string version(void);
    void debug(std::string);
    void add(std::string);
    void del(std::string);
    void init_tables(void);
    void init_table_seen(void);

   private:
     db::sqlite *db;
     WBS::Bot *bot;
  };
};
#endif
