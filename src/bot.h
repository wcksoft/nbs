// wbs.h 5.0.0
// Desc.: This is mainly for runtime variables.
// (c) 2011-2018 Wicked Software

#ifndef _WBS_BOT_H_
#define _WBS_BOT_H_

#include <iostream>
#include <string>

namespace WBS {
  class Bot {
   public:
    Bot(void);
    ~Bot(void);
    std::string version(void);
    bool connect(std::string, int);
    void debug(std::string);
    void show(std::string);
    std::string get_botname(void);
    std::string get_identd(void);
    std::string get_realname(void);
    std::string get_startchan(void);
    std::string get_server(void);
    int get_port(void);
    void set_botname(std::string);
    std::string kernel(void);
    std::string arch(void);
    std::string hostname(void);

   private:
     std::string botname;
     std::string identd;
     std::string realname;
     std::string startchan;
     std::string server;
     int port;
  };
};
#endif
