// WBS: seen.cpp 0.1.0
// Desc.:
// (c) 2018 Wicked Software

#include <iostream>
#include <string>
#include "seen.h"
#include "db/sqlite.h"
#include "bot.h"

#ifndef DEBUG
 #define DEBUG true
#endif

using namespace std;

namespace WBS {
  seen::seen(db::sqlite*& db, WBS::Bot*& bot) {
    this->db = db;
    this->bot = bot;
  }
  seen::seen(void) {  }
  seen::~seen(void) {  }
  std::string seen::version(void) { return "0.1.0"; }
  void seen::debug(std::string data) { if (DEBUG) { if ((data != "\n") && (data != "\r") && (data != "\0")) { cout<<">>>DEBUG: "<<data; } } }
  void seen::init_tables(void) {
    this->init_table_seen();
  }
  void seen::init_table_seen(void) {
    char *err = 0;
    string query = "CREATE TABLE IF NOT EXISTS seen (\
                   id TEXT PRIMARY KEY,\
                   nick TEXT NOT NULL UNIQUE,\
                   timestamp NUMERIC NOT NULL,\
                   doing TEXT NOT NULL);";
    this->db->query(query);
  }

}
