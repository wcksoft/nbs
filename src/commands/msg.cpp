// WBS: msg.cpp 0.1.0
// Desc.:
// (c) 2018 Wicked Software

#include <boost/algorithm/string.hpp>
#include <iostream>
#include <string>
#include <vector>
#include <cstring>
#include <ctime>
#include "../lib/socket_client.h"
#include "msg.h"
#include "../commands.h"
#include "../bot.h"

#ifndef DEBUG
 #define DEBUG true
#endif
#ifndef WBS_CHANCMD_PREFIX
 #define WBS_MSGCMD_PREFIX (std::string)"."
#endif

using namespace std;

namespace WBS {
  msgcom::msgcom(Socket::client*& sock, WBS::Commands*& send, WBS::Bot*& bot) {
    this->sock = sock;
    this->send = send;
    this->bot = bot;
  }
  msgcom::msgcom(void) {  }
  msgcom::~msgcom(void) {  }
  std::string msgcom::version(void) { return "0.1.0"; }
  void msgcom::debug(std::string data) { if (DEBUG) { if ((data != "\n") && (data != "\r") && (data != "\0")) { cout<<">>>DEBUG: "<<data; } } }
  void msgcom::show(std::string data) { if (DEBUG) { if ((data != "\n") && (data != "\r") && (data != "\0")) { cout<<data; } } }

  std::string msgcom::strip(std::string text, char delchar) {
    const char * chartxt = text.c_str();
    string newtxt = "";
    if (strlen(chartxt) > 0) {
     for (int i = 0; chartxt[i] != '\0'; i++) {
       if (chartxt[i] != delchar) { newtxt += chartxt[i]; }
     }
    }
    return newtxt;
  }

  void msgcom::commands(std::string user, std::string command, std::string arguments) {
    //cout<<user<<" "<<command<<" "<<arguments<<endl;
    std::vector<std::string> nick;
    boost::split(nick, user, [](char c){return c == '!';});
    //cout<<nick[0]<<" "<<command<<" "<<arguments<<endl;
    command = this->strip(this->strip(command, '\n'), '\r');
    if (command == WBS_MSGCMD_PREFIX+"version") {
      this->send->msg(nick[0], "Running WBS 5.0.0");
    } else if (command == WBS_MSGCMD_PREFIX+"whoami") {
      this->send->msg(nick[0], "You are "+user);
    } else if (command == WBS_MSGCMD_PREFIX+"kernel") {
      this->send->msg(nick[0], this->bot->kernel());
    } else if (command == WBS_MSGCMD_PREFIX+"arch") {
      this->send->msg(nick[0], this->bot->arch());
    } else if (command == WBS_MSGCMD_PREFIX+"hostname") {
      this->send->msg(nick[0], this->bot->hostname());
    } else if (command == WBS_MSGCMD_PREFIX+"join") {
      this->send->msg(nick[0], "Joining "+arguments);
      this->send->join(arguments);
    } else if (command == WBS_MSGCMD_PREFIX+"part") {
      this->send->msg(nick[0], "Leaving "+arguments);
      this->send->part(arguments);
    }
    return;
  }

}
