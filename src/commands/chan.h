// WBS: msg.h 0.1.0
// Desc.: This is used for display purposes.
// (c) 2018 Wicked Software

#ifndef _WBS_CHANCOM_H_
#define _WBS_CHANCOM_H_

#include <iostream>
#include <string>
#include "../lib/socket_client.h"
#include "../commands.h"
#include "../bot.h"

namespace WBS {
  class chancom {
   public:
    chancom(Socket::client*&, WBS::Commands*&, WBS::Bot*&);
    chancom(void);
    ~chancom(void);
    void debug(std::string);
    void show(std::string);
    void commands(std::string, std::string, std::string, std::string);
    std::string strip(std::string, char);

   private:
     Socket::client *sock;
     WBS::Commands *send;
     WBS::Bot *bot;
     void version(std::string);
     void kernel(std::string);
     void arch(std::string);
     void hostname(std::string);
     void whoami(std::string, std::string);
     void info(std::string);
     void seen(void);
     void ban(void);
     void unban(void);
     void voice(void);
     void devoice(void);
     void uptime(void);
     void topic(void);
     void kick(void);
     void mode(void);
  };
};
#endif
