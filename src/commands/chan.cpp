// WBS: msg.cpp 0.1.0
// Desc.:
// (c) 2018 Wicked Software

#include <boost/algorithm/string.hpp>
#include <iostream>
#include <string>
#include <vector>
#include <cstring>
#include <ctime>
#include "../lib/socket_client.h"
#include "chan.h"
#include "../commands.h"
#include "../bot.h"

#ifndef DEBUG
 #define DEBUG true
#endif
#ifndef WBS_CHANCMD_PREFIX
 #define WBS_CHANCMD_PREFIX (std::string)"!"
#endif

using namespace std;

namespace WBS {
  chancom::chancom(Socket::client*& sock, WBS::Commands*& send, WBS::Bot*& bot) {
    this->sock = sock;
    this->send = send;
    this->bot = bot;
  }
  chancom::chancom(void) {  }
  chancom::~chancom(void) {  }
  void chancom::debug(std::string data) { if (DEBUG) { if ((data != "\n") && (data != "\r") && (data != "\0")) { cout<<">>>DEBUG: "<<data; } } }
  void chancom::show(std::string data) { if (DEBUG) { if ((data != "\n") && (data != "\r") && (data != "\0")) { cout<<data; } } }

  std::string chancom::strip(std::string text, char delchar) {
    const char * chartxt = text.c_str();
    string newtxt = "";
    if (strlen(chartxt) > 0) {
     for (int i = 0; chartxt[i] != '\0'; i++) {
       if (chartxt[i] != delchar) { newtxt += chartxt[i]; }
     }
    }
    return newtxt;
  }

  void chancom::commands(std::string user, std::string chan, std::string command, std::string arguments) {
    //cout<<user<<" "<<command<<" "<<arguments<<endl;
    std::vector<std::string> nick;
    boost::split(nick, user, [](char c){return c == '!';});
    //cout<<nick[0]<<" "<<command<<" "<<arguments<<endl;
    command = this->strip(this->strip(command, '\n'), '\r');
    if (command == WBS_CHANCMD_PREFIX+"version") {
      this->version(chan);
    } else if (command == WBS_CHANCMD_PREFIX+"whoami") {
      this->whoami(chan, user);
    } else if (command == WBS_CHANCMD_PREFIX+"kernel") {
      this->kernel(chan);
    } else if (command == WBS_CHANCMD_PREFIX+"arch") {
      this->arch(chan);
    } else if (command == WBS_CHANCMD_PREFIX+"hostname") {
      this->hostname(chan);
    } else if (command == WBS_CHANCMD_PREFIX+"info") {
      this->info(chan);
    } else if (command == WBS_CHANCMD_PREFIX+"ban") {
      this->ban();
    } else if (command == WBS_CHANCMD_PREFIX+"unban") {
      this->unban();
    } else if (command == WBS_CHANCMD_PREFIX+"kick") {
      this->kick();
    } else if (command == WBS_CHANCMD_PREFIX+"voice") {
      this->voice();
    } else if (command == WBS_CHANCMD_PREFIX+"devoice") {
      this->devoice();
    } else if (command == WBS_CHANCMD_PREFIX+"mode") {
      this->mode();
    } else if (command == WBS_CHANCMD_PREFIX+"uptime") {
      this->uptime();
    } else if (command == WBS_CHANCMD_PREFIX+"topic") {
      this->topic();
    } else if (command == WBS_CHANCMD_PREFIX+"seen") {
      this->seen();
//    } else if (command == WBS_CHANCMD_PREFIX+"join") {
//      this->send->msg(chan, "Joining "+arguments);
//      this->send->join(arguments);
//    } else if (command == WBS_CHANCMD_PREFIX+"part") {
//      this->send->msg(chan, "Leaving "+arguments);
//      this->send->part(arguments);
    }
    return;
  }
  void chancom::info(std::string chan) { this->send->msg(chan, "Valid commands are: !version !kernel !arch !hostname !ban !kick !unban !voice !devoice !mode !uptime !topic !seen"); return; }
  void chancom::version(std::string chan) { this->send->msg(chan, "Running WBS 5.0.0"); return; }
  void chancom::whoami(std::string chan, std::string user) { this->send->msg(chan, "You are "+user); return; }
  void chancom::kernel(std::string chan) { this->send->msg(chan, this->bot->kernel()); return; }
  void chancom::arch(std::string chan) { this->send->msg(chan, this->bot->arch()); return; }
  void chancom::hostname(std::string chan) { this->send->msg(chan, this->bot->hostname()); return; }
  void chancom::ban(void) { return; }
  void chancom::unban(void) { return; }
  void chancom::kick(void) { return; }
  void chancom::voice(void) { return; }
  void chancom::devoice(void) { return; }
  void chancom::mode(void) { return; }
  void chancom::uptime(void) { return; }
  void chancom::topic(void) { return; }
  void chancom::seen(void) { return; }
}
