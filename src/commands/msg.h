// WBS: msg.h 0.1.0
// Desc.: This is used for display purposes.
// (c) 2018 Wicked Software

#ifndef _WBS_MSGCOM_H_
#define _WBS_MSGCOM_H_

#include <iostream>
#include <string>
#include "../lib/socket_client.h"
#include "../commands.h"
#include "../bot.h"

namespace WBS {
  class msgcom {
   public:
    msgcom(Socket::client*&, WBS::Commands*&, WBS::Bot*&);
    msgcom(void);
    ~msgcom(void);
    std::string version(void);
    void debug(std::string);
    void show(std::string);
    void commands(std::string, std::string, std::string);
    std::string strip(std::string, char);

   private:
     Socket::client *sock;
     WBS::Commands *send;
     WBS::Bot *bot;
  };
};
#endif
