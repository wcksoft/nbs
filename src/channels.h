// WBS: channels.h 0.1.0
// Desc.: This is used for display purposes.
// (c) 2018 Wicked Software

#ifndef _WBS_CHANNELS_H_
#define _WBS_CHANNELS_H_

#include <iostream>
#include <string>
#include "db/sqlite.h"
#include "bot.h"
#include "commands.h"

namespace WBS {
  class channels {
   public:
    channels(db::sqlite*&, WBS::Bot*&, WBS::Commands*&);
    channels(void);
    ~channels(void);
    std::string version(void);
    void debug(std::string);
    void add(std::string, std::string);
    void del(std::string);
    void edit(std::string, std::string);
    void edit_access(std::string, std::string, int, int, int);
    bool valid(std::string);
    void join_all(void);
    int count(void);
    void init_tables(void);

   private:
     db::sqlite *db;
     WBS::Bot *bot;
     WBS::Commands *send;
     std::string getUUID(void);
     void init_table_chans(void);
     void init_table_chans_access(void);
  };
};
#endif
