// WBS: commands.cpp 0.1.0
// Desc.:
// (c) 2018 Wicked Software

#include <boost/algorithm/string.hpp>
#include <iostream>
#include <string>
#include <vector>
#include <cstring>
#include <ctime>
#include "commands.h"
#include "lib/socket_client.h"

#ifndef DEBUG
 #define DEBUG true
#endif

using namespace std;

namespace WBS {
  Commands::Commands(Socket::client *& sock) {
    this->sock = sock;
  }
  Commands::Commands(void) { }
  Commands::~Commands(void) {  }
  std::string Commands::version(void) { return "0.1.0"; }
  void Commands::debug(std::string data) { if (DEBUG) { if ((data != "\n") && (data != "\r") && (data != "\0")) { cout<<">>>DEBUG: "<<data; } } }
  void Commands::show(std::string data) { if (DEBUG) { if ((data != "\n") && (data != "\r") && (data != "\0")) { cout<<data; } } }

  void Commands::user(std::string nick, std::string identd, std::string host, std::string realname) { this->sock->send_data("USER "+nick+" "+identd+" "+host+" :"+realname+"\r\n"); }
  void Commands::nick(std::string nick) { this->sock->send_data("NICK "+nick+"\r\n"); }
  void Commands::join(std::string chan) { this->sock->send_data("JOIN "+chan+"\r\n"); }
  void Commands::part(std::string chan) { this->sock->send_data("PART "+chan+"\r\n"); }
  void Commands::quit(std::string msg) { this->sock->send_data("QUIT :"+msg+"\r\n"); }
  void Commands::op(std::string chan, std::string victim) {  }
  void Commands::deop(std::string chan, std::string victim) { }
  void Commands::voice(std::string chan, std::string victim) { }
  void Commands::devoice(std::string chan, std::string victim) { }
  void Commands::ban(std::string chan, std::string mask) { }
  void Commands::unban(std::string chan, std::string mask) { }
  void Commands::kick(std::string chan, std::string victim, std::string reason) { }
  void Commands::mode(std::string chan, std::string modestring) { }
  void Commands::whois(std::string nick) { }
  void Commands::whowas(std::string nick) { }
  void Commands::links(void) { }
  void Commands::server_pong(std::string data) { this->sock->send_data("PONG :"+data.substr(1)+"\r\n"); }
  void Commands::ctcp_version(std::string user) {
    user = user.substr(1);
    std::vector<std::string> nick;
    boost::split(nick, user, [](char c){return c == '!';});
    if (DEBUG) { cout<<"Replied to "<<nick[0]<<" CTCP Version"<<endl; }
    this->sock->send_data("NOTICE "+nick[0]+" :\001VERSION WBS 5.0.0\001\r");
    return;
  }
  void Commands::ctcp_time(std::string user) {
    user = user.substr(1);
    std::vector<std::string> nick;
    boost::split(nick, user, [](char c){return c == '!';});
    if (DEBUG) { cout<<"Replied to "<<nick[0]<<" CTCP Time"<<endl; }
    time_t tt;
    struct tm * ti;
    time (&tt);
    ti = localtime(&tt);
    this->sock->send_data("NOTICE "+nick[0]+" :\001TIME "+asctime(ti)+"\001\r");
    return;
  }
  void Commands::ctcp_ping(std::string user, std::string time) {
    user = user.substr(1);
    std::vector<std::string> nick;
    boost::split(nick, user, [](char c){return c == '!';});
    if (DEBUG) { cout<<"Replied to "<<nick[0]<<" CTCP Ping"<<endl; }
    this->sock->send_data("NOTICE "+nick[0]+" :\001PING "+time+"\001\r");
    return;
  }
  void Commands::ctcp_clientinfo(std::string user) {
    user = user.substr(1);
    std::vector<std::string> nick;
    boost::split(nick, user, [](char c){return c == '!';});
    if (DEBUG) { cout<<"Replied to "<<nick[0]<<" CTCP ClientInfo"<<endl; }
    this->sock->send_data("NOTICE "+nick[0]+" :\001CLIENTINFO VERSION PING TIME CLIENTINFO  --Use 'CLIENTINFO <command>' for a description\001\r");
    return;
  }
  void Commands::ctcp_clientinfo_ci(std::string user) {
    user = user.substr(1);
    std::vector<std::string> nick;
    boost::split(nick, user, [](char c){return c == '!';});
    if (DEBUG) { cout<<"Replied to "<<nick[0]<<" CTCP ClientInfo clientinfo"<<endl; }
    this->sock->send_data("NOTICE "+nick[0]+" :\001CLIENTINFO CLIENTINFO: With no parameters, lists supported CTCP commands, while 'CLIENTINFO <command>' describes <command>.\001\r");
    return;
  }
  void Commands::ctcp_clientinfo_ver(std::string user) {
    user = user.substr(1);
    std::vector<std::string> nick;
    boost::split(nick, user, [](char c){return c == '!';});
    if (DEBUG) { cout<<"Replied to "<<nick[0]<<" CTCP ClientInfo version"<<endl; }
    this->sock->send_data("NOTICE "+nick[0]+" :\001CLIENTINFO VERSION: Returns the version of this client.\001\r");
    return;
  }
  void Commands::ctcp_clientinfo_ping(std::string user) {
    user = user.substr(1);
    std::vector<std::string> nick;
    boost::split(nick, user, [](char c){return c == '!';});
    if (DEBUG) { cout<<"Replied to "<<nick[0]<<" CTCP ClientInfo"<<endl; }
    this->sock->send_data("NOTICE "+nick[0]+" :\001CLIENTINFO PING: Returns given parameters without parsing them.\001\r");
    return;
  }
  void Commands::ctcp_clientinfo_time(std::string user) {
    user = user.substr(1);
    std::vector<std::string> nick;
    boost::split(nick, user, [](char c){return c == '!';});
    if (DEBUG) { cout<<"Replied to "<<nick[0]<<" CTCP ClientInfo time"<<endl; }
    this->sock->send_data("NOTICE "+nick[0]+" :\001CLIENTINFO TIME: Returns the current local time.\001\r");
    return;
  }
  void Commands::msg(std::string nick, std::string text) { this->sock->send_data("PRIVMSG "+nick+" :"+text+"\r"); }
  //void Commands::msgchan(std::string chan, std::string text) { this->sock->send_data("PRIVMSG "+chan+" :"+text+"\r"); }
  bool Commands::connect(std::string server, int port) {
    if (this->sock->conn(server, port)) {
      if (DEBUG) { cout<<">>>DEBUG: Connected to "+server+":"<<port<<"\n"; }
      return true;
    } else {
      this->debug("Could not connect to irc server.\n");
      return false;
    }
  }
}
