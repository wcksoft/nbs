// WBS: events.cpp 0.1.0
// Desc.:
// (c) 2018 Wicked Software

#include <boost/algorithm/string.hpp>
#include <iostream>
#include <string>
#include <vector>
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include "events.h"
#include "channels.h"

#ifndef DEBUG
 #define DEBUG true
#endif

using namespace std;

namespace WBS {
  Events::Events(WBS::Bot*& bot, WBS::Commands*& send, WBS::channels*& channels) {
    this->bot = bot;
    this->send = send;
    this->channels = channels;
  }
  Events::Events(void) { }
  Events::~Events(void) {  }
  std::string Events::version(void) { return "0.1.0"; }
  void Events::debug(std::string data) { if (DEBUG) { if ((data != "\n") && (data != "\r") && (data != "\0")) { cout<<">>>DEBUG: "<<data; } } }
  void Events::show(std::string data) { if (DEBUG) { if ((data != "\n") && (data != "\r") && (data != "\0")) { cout<<data; } } }

  std::string Events::strip(std::string text, char delchar) {
    const char * chartxt = text.c_str();
    string newtxt = "";
    if (strlen(chartxt) > 0) {
     for (int i = 0; chartxt[i] != '\0'; i++) {
       if (chartxt[i] != delchar) { newtxt += chartxt[i]; }
     }
    }
    return newtxt;
  }
  void Events::raw_strip3(std::string data) {
    data = data.substr(data.find_first_of(" ")+1);
    data = data.substr(data.find_first_of(" ")+1);
    data = data.substr(data.find_first_of(" ")+1);
    if (data.c_str()[0] == ':') {
      this->show(data.substr(1));
    } else {
      this->show(data);
    }
  }
  void Events::raw_strip4(std::string data) {
    data = data.substr(data.find_first_of(" ")+1);
    data = data.substr(data.find_first_of(" ")+1);
    data = data.substr(data.find_first_of(" ")+1);
    data = data.substr(data.find_first_of(" ")+1);
    if (data.c_str()[0] == ':') {
      this->show(data.substr(1));
    } else {
      this->show(data);
    }
  }
  void Events::raw_strip5(std::string data) {
    data = data.substr(data.find_first_of(" ")+1);
    data = data.substr(data.find_first_of(" ")+1);
    data = data.substr(data.find_first_of(" ")+1);
    data = data.substr(data.find_first_of(" ")+1);
    data = data.substr(data.find_first_of(" ")+1);
    if (data.c_str()[0] == ':') {
      this->show(data.substr(1));
    } else {
      this->show(data);
    }
  }
  void Events::join(std::string user, std::string chan) { cout<<user.substr(1)<<" joined "<<this->strip(this->strip(chan.substr(1), '\r'), '\n')<<endl; }
  void Events::part(std::string user, std::string chan) { cout<<user.substr(1)<<" left "<<this->strip(this->strip(chan.substr(1), '\r'), '\n')<<endl; }
  void Events::quit(std::string user, std::string msg) { cout<<user.substr(1)<<" quit irc. -"<<this->strip(this->strip(msg.substr(1), '\r'), '\n')<<endl; }
  void Events::mode(std::string user, std::string chan, std::string mode) { cout<<user.substr(1)<<" has set mode "<<mode<<" on "<<this->strip(this->strip(chan.substr(1), '\r'), '\n')<<endl; }
  void Events::kick(std::string user, std::string chan, std::string victim, std::string reason) { cout<<user.substr(1)<<" left "<<this->strip(this->strip(chan.substr(1), '\r'), '\n')<<endl; }
  void Events::op(std::string user, std::string chan, std::string victim) { }
  void Events::deop(std::string user, std::string chan, std::string victim) { }
  void Events::voice(std::string user, std::string chan, std::string victim) { }
  void Events::devoice(std::string user, std::string chan, std::string victim) { }
  void Events::ban(std::string user, std::string chan, std::string mask) { }
  void Events::unban(std::string user, std::string chan, std::string mask) { }
  void Events::topic(std::string user, std::string chan, std::string topictext) { }
  void Events::raw001(std::string data) {
    //RPL_WELCOME - RFC2812
    this->raw_strip3(data);
    this->send->join(this->bot->get_startchan());
    this->channels->join_all();
  }
  void Events::raw002(std::string data) { this->raw_strip3(data); }  //RFC2812
  void Events::raw003(std::string data) { this->raw_strip3(data); }  //RFC2812
  void Events::raw004(std::string data) { this->raw_strip3(data); }  //RFC2812
  void Events::raw005(std::string data) { this->raw_strip3(data); }  //RFC2812
  //void Events::raw006(std::string data) {  }  //unreal
  //void Events::raw007(std::string data) {  }  //unreal
  //void Events::raw008(std::string data) {  }  //ircu
  //void Events::raw009(std::string data) {  }  //ircu
  //void Events::raw010(std::string data) {  }  //ircu
  //void Events::raw014(std::string data) { }  //hybrid?
  //void Events::raw015(std::string data) { }  //ircu
  //void Events::raw016(std::string data) { }  //ircu
  //void Events::raw017(std::string data) { }  //ircu
  //void Events::raw042(std::string data) { }  //ircnet
  //void Events::raw043(std::string data) { }  //ircnet
  //void Events::raw050(std::string data) { }  //aircd
  //void Events::raw051(std::string data) { }  //aircd
  void Events::raw200(std::string data) { }  //RFC1459
  void Events::raw201(std::string data) { }  //RFC1459
  void Events::raw202(std::string data) { }  //RFC1459
  void Events::raw203(std::string data) { }  //RFC1459
  void Events::raw204(std::string data) { }  //RFC1459
  void Events::raw205(std::string data) { }  //RFC1459
  void Events::raw206(std::string data) { }  //RFC1459
  void Events::raw207(std::string data) { }  //RFC2812
  void Events::raw208(std::string data) { }  //RFC1459
  void Events::raw209(std::string data) { }  //RFC2812
  void Events::raw210(std::string data) { }  //RFC2812
  void Events::raw211(std::string data) { }  //RFC1459
  void Events::raw212(std::string data) { }  //RFC1459
  void Events::raw213(std::string data) { }  //RFC1459
  void Events::raw214(std::string data) { }  //RFC1459
  void Events::raw215(std::string data) { }  //RFC1459
  void Events::raw216(std::string data) { }  //RFC1459
  void Events::raw217(std::string data) { }  //RFC1459
  void Events::raw218(std::string data) { }  //RFC1459
  void Events::raw219(std::string data) { }  //RFC1459
  //void Events::raw220(std::string data) { }  //hybrid or bahamut/unreal
  void Events::raw221(std::string data) { }  //RFC1459
  //void Events::raw222(std::string data) { }
  //void Events::raw223(std::string data) { }
  //void Events::raw224(std::string data) { }
  //void Events::raw225(std::string data) { }
  //void Events::raw226(std::string data) { }
  //void Events::raw227(std::string data) { }
  //void Events::raw228(std::string data) { }
  void Events::raw231(std::string data) { }  //RFC1459
  void Events::raw232(std::string data) { }  //RFC1459
  void Events::raw233(std::string data) { }  //RFC1459
  void Events::raw234(std::string data) { }  //RFC2812
  void Events::raw235(std::string data) { }  //RFC2812
  //void Events::raw236(std::string data) { }
  //void Events::raw237(std::string data) { }
  //void Events::raw238(std::string data) { }
  //void Events::raw239(std::string data) { }
  void Events::raw240(std::string data) { }  //RFC2812
  void Events::raw241(std::string data) { }  //RFC1459
  void Events::raw242(std::string data) { }  //RFC1459
  void Events::raw243(std::string data) { }  //RFC1459
  void Events::raw244(std::string data) { }  //RFC1459
  //void Events::raw245(std::string data) { }  //hybrid or bahamut/unreal
  void Events::raw246(std::string data) { }  //RFC2812
  void Events::raw247(std::string data) { }  //RFC2812
  //void Events::raw248(std::string data) { }
  //void Events::raw249(std::string data) { }
  void Events::raw250(std::string data) { this->raw_strip3(data); }  //RFC2812
  void Events::raw251(std::string data) { this->raw_strip3(data); }  //RFC1459
  void Events::raw252(std::string data) { this->raw_strip4(data); }  //RFC1459
  void Events::raw253(std::string data) {  }  //RFC1459
  void Events::raw254(std::string data) { this->raw_strip4(data); }  //RFC1459
  void Events::raw255(std::string data) { this->raw_strip3(data); }  //RFC1459
  void Events::raw256(std::string data) { }  //RFC1459
  void Events::raw257(std::string data) { }  //RFC1459
  void Events::raw258(std::string data) { }  //RFC1459
  void Events::raw259(std::string data) { }  //RFC1459
  void Events::raw261(std::string data) { }  //RFC1459
  void Events::raw262(std::string data) { }  //RFC2812
  void Events::raw263(std::string data) { }  //RFC2812
  void Events::raw265(std::string data) { this->raw_strip5(data); }
  void Events::raw266(std::string data) { this->raw_strip5(data); }
  //void Events::raw267(std::string data) { }
  //void Events::raw268(std::string data) { }
  //void Events::raw269(std::string data) { }
  //void Events::raw270(std::string data) { }
  //void Events::raw271(std::string data) { }
  //void Events::raw272(std::string data) { }
  //void Events::raw273(std::string data) { }
  //void Events::raw274(std::string data) { }
  //void Events::raw275(std::string data) { }
  //void Events::raw276(std::string data) { }
  //void Events::raw277(std::string data) { }
  //void Events::raw278(std::string data) { }
  //void Events::raw280(std::string data) { }
  //void Events::raw281(std::string data) { }
  //void Events::raw282(std::string data) { }
  //void Events::raw283(std::string data) { }
  //void Events::raw284(std::string data) { }
  //void Events::raw285(std::string data) { }
  //void Events::raw286(std::string data) { }
  //void Events::raw287(std::string data) { }
  //void Events::raw288(std::string data) { }
  //void Events::raw289(std::string data) { }
  //void Events::raw290(std::string data) { }
  //void Events::raw291(std::string data) { }
  //void Events::raw292(std::string data) { }
  //void Events::raw293(std::string data) { }
  //void Events::raw294(std::string data) { }
  //void Events::raw295(std::string data) { }
  //void Events::raw296(std::string data) { }
  //void Events::raw299(std::string data) { }
  void Events::raw300(std::string data) { }  //RFC1459
  void Events::raw301(std::string data) { }  //RFC1459
  void Events::raw302(std::string data) { }  //RFC1459
  void Events::raw303(std::string data) { }  //RFC1459
  //void Events::raw304(std::string data) { }
  void Events::raw305(std::string data) { }  //RFC1459
  void Events::raw306(std::string data) { }  //RFC1459
  //void Events::raw307(std::string data) { }
  //void Events::raw308(std::string data) { }
  //void Events::raw309(std::string data) { }
  //void Events::raw310(std::string data) { }
  void Events::raw311(std::string data) { }  //RFC1459
  void Events::raw312(std::string data) { }  //RFC1459
  void Events::raw313(std::string data) { }  //RFC1459
  void Events::raw314(std::string data) { }  //RFC1459
  void Events::raw315(std::string data) { }  //RFC1459
  void Events::raw316(std::string data) { }  //RFC1459
  void Events::raw317(std::string data) { }  //RFC1459
  void Events::raw318(std::string data) { }  //RFC1459
  void Events::raw319(std::string data) { }  //RFC1459
  //void Events::raw320(std::string data) { }
  void Events::raw321(std::string data) { }  //RFC1459
  void Events::raw322(std::string data) { }  //RFC1459
  void Events::raw323(std::string data) { }  //RFC1459
  void Events::raw324(std::string data) { }  //RFC1459
  void Events::raw325(std::string data) { }  //RFC2812
  void Events::raw332(std::string data) { this->raw_strip3(data); }
  void Events::raw333(std::string data) { this->raw_strip3(data); }
  void Events::raw353(std::string data) { this->raw_strip3(data); }
  void Events::raw366(std::string data) { this->raw_strip3(data); }
  void Events::raw372(std::string data) { this->raw_strip3(data); }
  void Events::raw375(std::string data) { this->raw_strip3(data); }
  void Events::raw376(std::string data) { this->raw_strip3(data); } //RPL_ENDOFMOTD - RFC1459
  void Events::raw381(std::string data) { }  //RFC1459
  void Events::raw382(std::string data) { }  //RFC1459
  void Events::raw383(std::string data) { }  //RFC2812
  void Events::raw384(std::string data) { }  //RFC1459
  //void Events::raw385(std::string data) { }  //hybrid & unreal
  void Events::raw391(std::string data) { }  //RFC1459
  void Events::raw392(std::string data) { }  //RFC1459
  void Events::raw393(std::string data) { }  //RFC1459
  void Events::raw394(std::string data) { }  //RFC1459
  void Events::raw395(std::string data) { }  //RFC1459
  void Events::raw400(std::string data) { }  //
  void Events::raw401(std::string data) { }  //RFC1459
  void Events::raw402(std::string data) { }  //RFC1459
  void Events::raw403(std::string data) { }  //RFC1459
  void Events::raw404(std::string data) { }  //RFC1459
  void Events::raw405(std::string data) { }  //RFC1459
  void Events::raw406(std::string data) { }  //RFC1459
  void Events::raw407(std::string data) { }  //RFC1459
  void Events::raw408(std::string data) { }  //RFC1459
  void Events::raw409(std::string data) { }  //RFC1459
  void Events::raw411(std::string data) { }  //RFC1459
  void Events::raw412(std::string data) { }  //RFC1459
  void Events::raw413(std::string data) { }  //RFC1459
  void Events::raw414(std::string data) { }  //RFC1459
  void Events::raw415(std::string data) { }  //RFC2812
  void Events::raw421(std::string data) { }  //RFC1459
  void Events::raw422(std::string data) { }  //RFC1459
  void Events::raw423(std::string data) { }  //RFC1459
  void Events::raw424(std::string data) { }  //RFC1459
  void Events::raw431(std::string data) { }  //RFC1459
  void Events::raw432(std::string data) { }  //ERR_ERRONEUSNICKNAME RFC1459
  void Events::raw433(std::string nick, std::string reason) {
    //ERR_NICKNAMEINUSE RFC1459
    string newname;
    if (this->bot->get_botname().length() >= 9) {
      newname = this->bot->get_botname().substr(0,7) + std::to_string(rand()%100);
    } else {
      if (this->bot->get_botname().length() == 8) {
        newname = this->bot->get_botname().substr(0,7) + std::to_string(rand()%100);
      } else {
        newname = this->bot->get_botname() + std::to_string(rand()%100);
      }
    }
    this->bot->set_botname(newname);
    this->send->nick(newname);
  }
  void Events::raw436(std::string data) { }  //ERR_NICKCOLLISION RFC1459
  void Events::raw437(std::string data) { }  //RFC2812
  void Events::raw441(std::string data) { }  //RFC1459
  void Events::raw442(std::string data) { }  //RFC1459
  void Events::raw443(std::string data) { }  //RFC1459
  void Events::raw444(std::string data) { }  //RFC1459
  void Events::raw445(std::string data) { }  //RFC1459
  void Events::raw446(std::string data) { }  //RFC1459
  void Events::raw451(std::string data) { }  //RFC1459
  void Events::raw461(std::string data) { }  //RFC1459
  void Events::raw462(std::string data) { }  //RFC1459
  void Events::raw463(std::string data) { }  //RFC1459
  void Events::raw464(std::string data) { }  //RFC1459
  void Events::raw465(std::string data) { }  //RFC1459
  void Events::raw466(std::string data) { }  //RFC1459
  void Events::raw467(std::string data) { }  //RFC1459
  void Events::raw471(std::string data) { }  //RFC1459
  void Events::raw472(std::string data) { }  //RFC1459
  void Events::raw473(std::string data) { }  //RFC1459
  void Events::raw474(std::string data) { }  //RFC1459
  void Events::raw475(std::string data) { }  //RFC1459
  void Events::raw476(std::string data) { }  //RFC2812
  void Events::raw477(std::string data) { }  //RFC2812
	void Events::raw478(std::string data) { }  //RFC2812
  void Events::raw481(std::string data) { }  //RFC1459
  void Events::raw482(std::string data) { }  //RFC1459
  void Events::raw483(std::string data) { }  //RFC1459
  void Events::raw484(std::string data) { }  //RFC2812
  void Events::raw485(std::string data) { }  //RFC2812
  void Events::raw491(std::string data) { }  //RFC1459
  void Events::raw492(std::string data) { }  //RFC1459
  void Events::raw501(std::string data) { }  //RFC1459
  void Events::raw502(std::string data) { }  //RFC1459


}
